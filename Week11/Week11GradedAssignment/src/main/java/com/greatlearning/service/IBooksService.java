package com.greatlearning.service;

import java.util.List;

import com.greatlearning.bean.Books;
import com.greatlearning.exception.ProjectException;

public interface IBooksService {

	public Books addBook(Books book);

	public List<Books> getAllBooks();

	public Books getBookById(Integer bookId) throws ProjectException;

	public Books updateBook(Books book);

	public String deleteBookById(Integer bookId) throws ProjectException;

	public String addBooksToLike(Integer userId, Integer bookId) throws ProjectException;

	public List<Books> getLikedBooksByUserId(Integer userId) throws ProjectException;

	String deleteBooksFromLike(Integer userId, Integer bookId) throws ProjectException;

}
