package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.bean.Admin;
import com.greatlearning.bean.Books;
import com.greatlearning.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	
   public String adminRegistration(Admin adm) {
	   if(adminDao.existsById(adm.getEmail())) {
		   return "your Details Already Present";
	   }else {
		   adminDao.save(adm);
		   return "Details Saved Sucessfully";
	   }	   
   }
   
   public String checkAdminDetails(Admin adm) {
	   if(adminDao.existsById(adm.getEmail())) {
		   Admin a=adminDao.getById(adm.getEmail());
		   if(a.getPassword().equals(adm.getPassword())) {
			   return "You Logged In sucessfully";
		   }else {
			   return "Please Enter Correct Details";
		   }				   
	   }else {
		   return "Your Details Are Not Exists!!";
	   }
	   
   }

   public List<Admin> getAllAdminAvaliable(){
		return adminDao.findAll();
	}
}
   
   
