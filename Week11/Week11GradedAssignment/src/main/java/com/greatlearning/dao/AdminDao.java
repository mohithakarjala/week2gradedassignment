package com.greatlearning.dao;


	import org.springframework.data.jpa.repository.JpaRepository;

	import com.greatlearning.bean.Admin;

	public interface AdminDao extends JpaRepository<Admin, String> {

	}



