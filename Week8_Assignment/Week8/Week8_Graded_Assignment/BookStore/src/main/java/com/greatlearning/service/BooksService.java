package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.greatlearning.bean.*;
import com.greatlearning.dao.*;

public class BooksService {

	@Autowired
	BooksDao dao;

	public List<Books> getAllBooks() {
		return dao.getAllBooks();

	}
}
