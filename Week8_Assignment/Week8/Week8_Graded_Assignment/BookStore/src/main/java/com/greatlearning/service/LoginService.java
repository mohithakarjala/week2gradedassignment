package com.greatlearning.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.greatlearning.dao.*;

public class LoginService {

	@Autowired
	LoginDao dao ;

	public boolean validateUser(String username, String password) {
		return dao.validateUser(username, password);
	}
}
