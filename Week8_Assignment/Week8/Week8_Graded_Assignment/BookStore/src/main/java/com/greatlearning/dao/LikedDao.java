package com.greatlearning.dao;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.greatlearning.bean.*;

public class LikedDao {

	@Autowired
	private JdbcTemplate template;
	

	public boolean addBooks(int id, String name, String author, String url) throws DuplicateKeyException{
		String sql = "insert into Likelist values(?,?,?,?)";
		int i = template.update(sql, id , name , author, url);
		if (i!=0)
			return true;
		else
			throw new DuplicateKeyException("Book Already Exists.!");
	}
		
		public List<LikedBooks> getAllBooks() {
			String sql = "select id, name , author, url from Likelist";
			return template.query(sql, new LikedRowMapper());
		}
		
		public List<LikedBooks> getBook(int id) {
			String sql = "select id, name , author, url from Likelist where id = ?";
			return template.query(sql, new LikedRowMapper(), id );
		}
	
		public boolean deleteBook(int id) {
			String sql = "delete from Likelist where id = ?";
			int i = template.update(sql, id);
			if(i!=0)
				return true;
			else
				return false;
	}
}
