package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import com.greatlearning.bean.*;
import com.greatlearning.dao.*;

public class ReadLaterService {

	@Autowired
	ReadLaterDao dao;

	public List<ReadLaterBooks> getAllBooks() {
		return dao.getAllBooks();
	}

	public boolean addBooks(int id, String name, String author, String url) throws DuplicateKeyException {
		return dao.addBooks(id, name, author, url);
	}

	public boolean deleteBook(int id) {
		return dao.deleteBook(id);
	}
}
