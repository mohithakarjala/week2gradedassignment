package com.greatlearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.User;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer>{

}
