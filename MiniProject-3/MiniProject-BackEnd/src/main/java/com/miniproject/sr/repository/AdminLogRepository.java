package com.miniproject.sr.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.miniproject.sr.pojo.AdminLogin;

public interface AdminLogRepository extends CrudRepository<AdminLogin, Integer>{
	List<AdminLogin> findAll();
}



