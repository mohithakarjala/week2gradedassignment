package Week2_Coding_Assignment_HCL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

	//4.DataStructureB class 
public class DataStructureB {
	           public void cityNameCount(ArrayList<Employee> employees) {
	                      Set<String> l = new TreeSet<String>();
	                      ArrayList<String> a = new ArrayList<String>();
	                      for (Employee s : employees) {
	                                 a.add(s.getCity());
	                                 l.add(s.getCity());
	                      }
	                      for (String s : l) {
	                     System.out.print(s + "=" + Collections.frequency(a, s) + " ");
	                      }
	           }
	           public void monthlySalary(ArrayList<Employee> employees) {
	                      for (Employee e : employees) {	    	 
	System.out.print(e.getId() + "=" + e.getSalary() / 12 + " ");
	                      }
	           }
	}