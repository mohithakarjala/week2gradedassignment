package Week2_Coding_Assignment_HCL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

//5.Main class
public class Main {
           public static void main(String[] args) {
                ArrayList<Employee> list = new ArrayList<>();
//              System.out.println("enter how many employee address u need to add");
//        int n=s.nextInt();
//        for(int i=1;i<=n;i++){
//        }
                  Employee e1 = new Employee(1, "Aman", 20, 1100000, "IT", "Delhi");
                      list.add(e1);
                  Employee e2 = new Employee(2, "Bobby", 22, 500000, "HR", "Bombay");
                      list.add(e2);
                  Employee e3 = new Employee(3, "Zoe", 20, 750000, "Admin", "Delhi");
                      list.add(e3);
                  Employee e4 = new Employee(4, "Smitha", 21, 1000000, "IT", "Chennai");
                      list.add(e4);
                  Employee e5 = new Employee(5, "Smitha", 24, 1200000, "HR", "Bengaluru");
                      list.add(e5);
                      System.out.print("List of employees :" + list);                
                        System.out.println("\n");
                  DataStructureB b = new DataStructureB(); 
       System.out.println("Monthly Salary of employee along with their ID is :");
                      b.monthlySalary(list);
                          System.out.println("\n");
           System.out.println("Count of Employees from each city :");
                      b.cityNameCount(list);
                               System.out.println("\n");
                      DataStructureA a = new DataStructureA();
           System.out.println("Names of all employees in the sorted order are:" );
                       a.sortingNames(list);
                    
           }
}