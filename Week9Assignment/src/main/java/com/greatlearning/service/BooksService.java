package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.bean.Books;
import com.greatlearning.dao.BooksDao;

@Service
public class BooksService {
	@Autowired
	BooksDao booksDao;
	
	//Create Book
	public String storeBookDetails(Books book) {
		if(!booksDao.existsById(book.getId())) {
			booksDao.save(book);
			return "Book Stored Sucessfully";
		}else {
			return "This Book Already Exists!!";
		}
	}
	
	//Update Book Price By ID
	public String updateBookDetails(Books book) {
		if(booksDao.existsById(book.getId())) {
			Books bb=booksDao.getById(book.getId());
			if(bb.getPrice()==(book.getPrice())) {
				return "You are giving the old input";
			}else {
				bb.setPrice(book.getPrice());
				booksDao.saveAndFlush(bb);
				return "Book updated sucessfully";
			}
		}else {
			return "Book not Exists!!";			
		}						
	}
	
	//Retrieve
	public List<Books> getAllBooksAvaliable(){
		return booksDao.findAll();
	}
	
	//Get Book Details By Giving Id
	public Books findBooksById(int id) {
		if(booksDao.existsById(id)) {
			return booksDao.findById(id).get();
		}else {
			return null;
		}
		
	}
	
	//Deleted Book By ID
	public String deleteBookById(int id) {
		if(!booksDao.existsById(id)) {
			return "Book  Not Exists";
		}else {
			booksDao.deleteById(id);
			return "Book Deleted Sucessfully";
		}
	}
	
	
}
