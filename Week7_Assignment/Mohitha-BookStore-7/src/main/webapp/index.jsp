<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Index Page</title>
<style>
  
div{
  text-align:center;
  font-size: 40px;
  color:#1E90FF;
  width: 500px;
  height: 150px;
  padding: 80px;
  border: 8px solid red;
  margin-left: auto;
  margin-right: auto;
  background-color:#FFB6C1;
}
</style>
</head>
<body>
<%
		String userName = (String) request.getAttribute("userName");
		session.setAttribute("userName", userName);
		%>
<h1 style="font-size: 70px;text-align:center;color:#FF1493;">Welcome To Book Store</h1>
		<div>
		<a href="displayBooks.jsp">Display Books Without Login</a>
		<br> <a href="login.jsp">Login</a> <br>
		<a href="registration.jsp">New Registration</a> 
		</div>
</body>
</html>