package com.greatlearning.dao;

import java.sql.*;


import com.greatlearning.Dbresource.DbConnection;
import com.greatlearning.bean.Login;

public class LoginDao {
	
	public int storeLogin(Login login) {
		try {
			Connection con = DbConnection.getConnection();
			PreparedStatement pstmt = con.prepareStatement("insert into login values(?,?)");
			pstmt.setString(1, login.getUserName());
			pstmt.setString(2, login.getPassWord());
			
			return pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Store Method Exception "+e);
			return 0;
		}
	}
}