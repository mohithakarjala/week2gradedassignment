package com.greatlearning.bean;

public class Registration {
	private String userName, passWord, email;
	
	public Registration() {
		
	}

	public Registration(String userName, String passWord, String email) {
		
		this.userName = userName;
		this.passWord = passWord;
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUname(String uname) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassword(String password) {
		this.passWord = passWord;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Registration [userName=" + userName + ", passWord=" + passWord + ", email=" + email + "]";
	}
	
	
}
